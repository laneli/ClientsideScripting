Realtime Drawing with HTML5 Canvas, Node.js and Socket.io
=======================
This project shows how HTML5 Canvas works and how it can be linked with JavaScript for realtime drawing.

#What is Canvas?

Canvas is a new HTML5 element. It can be used easily with the <canvas> Tag. To insert a Canvas the following code is used:

<canvas id="myCanvas" width="578" height="200"></canvas>

The element itself has no functionality, for drawing with canvas it has to be accessed with the JavaScript Canvas API.
To access the Canvas with JavaScript a context is needed:

```
<script>
    var canvas = document.getElementById('myCanvas');
    var context = canvas.getContext('2d');
</script>
```

The context is the object which has the proper properties and methods for accessing the Canvas and it is responsible for the rendering either in 2D or in 3D with WebGL.

##Cavas API

The pure Canvas API delivers methods on a very abstract level. For every single operation there is an own method. For example to draw a line there are a few lines necessary:

```
<script>
      var canvas = document.getElementById('myCanvas');
      var context = canvas.getContext('2d');

      context.beginPath();
      context.moveTo(200, 150);
      context.lineTo(450, 50);
      context.stroke();
</script>
```

#Realtime Drawing with Canvas

This project will be a realime drawing app. For this purpose we need a server and a technology which handles the realtime stuff. For the server I will use node.js with express and for realtime the Socket.io, a node.js project.

##Basic Application

The first step is to install express and Socket.io in the project folder. For this purpose switch to the root folder of the project and type the commands ```npm install express``` and ```npm install socket.io```.

After installing the necessary components we will create our index.html. The index.html only contains the basic structure and the code for embedding the scripts. We also don't add the canvas tag here, it will be initialized later with JavaScript.
```
<html>
    <head>
        <title>HTML5 Canvas</title>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.event.drag-2.0.min.js"></script>
        <script src="http://localhost:8000/socket.io/socket.io.js"></script>
        <link rel="stylesheet" type="text/css" href="styles.css" />
    </head>
    <body>
        <div id="canvas"><!-- Canvas will be created here --></div>
    </body>
    <script src="js/draw.js"></script>
</html>
```

The next thing to do is to create the server for the realtime stuff. For this purpose we create a server.js file.
To use express and Socket.io we will need a few lines which includes the necessary components. At this point we also define our port number.
```
//App initialization
var express = require('express');
var app = express();
var socket = require('socket.io');

var port = 8000;
```

The next step is to create a session so Socket.io can identify the express session. For this purpose we need a cookie which contains the SID of our express session.
```
//Session handling
app.use(express.cookieParser());
app.use(express.session({secret: 'secret', key: 'express.sid'}));
```

Then we create our routes. The routes describe the path of the URI and which data will be sent to the client.
```
//Routes
app.get('/', function(req, res){
    res.sendfile('index.html');
});

app.get('/js/draw.js', function(req, res){
    res.sendfile('js/draw.js');
});

app.get('/styles.css', function(req, res) {
	res.sendfile('styles.css');
});

app.get('/js/jquery.event.drag-2.0.min.js', function(req, res) {
	res.sendfile('js/jquery.event.drag-2.0.min.js');
});
```

The last step on the serverside is to listen with express to our port and to bind the server to the socket. Additionally we create socket functions to receive and broadcast the data from and to the clients. 
```
//Listen for requests
var server = app.listen(port);
var io = socket.listen(server, {log: false});

//Socket
io.sockets.on('connection', function(socket) {
	socket.on('drawClick', function(data) {
		socket.broadcast.emit('draw', {
			x: data.x,
			y: data.y,
			type: data.type
		});
	});
});
```

Now our server is finished and we are going to create our clientside application. For this purpose we need a new draw.js file in the js folder. What we also need is the jquery.event.drag-2.0.min.js file. Make sure that you put it also in the js folder, or change the route to the file. It can be downloaded here: http://code.google.com/p/threedubmedia/downloads/detail?name=jquery.event.drag-2.0.min.js&can=2&q=

This file is responsible for the drag and drop events we will need later.

In the draw.js file the first thing to do is to create and configure our canvas.
```
//Create canvas element
var canvas = document.createElement('canvas');
canvas.id = 'myCanvas';
canvas.height = 400;
canvas.width = 600;
canvas.draggable = true;
var domCanvas = document.getElementById('canvas');
domCanvas.appendChild(canvas);

//Add 2D context
var context = canvas.getContext('2d');

//Settings for the line
context.fillStyle ='solid';
context.strokeStyle = '#000000';
context.lineWidth = 5;
context.lineCap = 'round';
```

On the clientside we also need a socket connection.
```
//Socket clientside
var socket = io.connect('http://localhost:8000/');

socket.on('draw', function(data) {
	return draw(data.x, data.y, data.type);
});
```

What is missing is the actual drawing functionality. For drawing on the canvas we use the following code:
```
//Drawing
draw = function(x, y, type) {

	if (type === 'dragstart') {
		context.beginPath();
		return context.moveTo(x, y);
	} else if (type === 'drag') {
		context.lineTo(x, y);
		return context.stroke();
	} else {
		return context.closePath();
	}
};
```

But we also have to recognize when the user is drawing. We have to create some events to handle this. The data which is sent via the socket is gathered from the event.
```
//Drawing events
$('canvas').on('drag dragstart dragend', function(event) {
	var offset, type, x, y;

	type = event.handleObj.type;
	offset = $(this).offset();
	event.offsetX = event.clientX - offset.left;
    event.offsetY = event.clientY - offset.top;
    x = event.offsetX;
    y = event.offsetY;

    draw(x, y, type);
    socket.emit('drawClick', {
    	x: x,
    	y: y,
    	type: type
    });
});
```

This is the basic drawing application. For styling I have created a styles.css with two styles:
```
body {
	background-color: #444444;
}

canvas {
	background-color: #fff;
}
```

##Additional features

After completing the basic application you will recognize it is a bit boring just drawing with one color and one size. We will add some features to accomplish this but we have to always keep in mind that we have to transfer all information over our sockets. So we need for all our functionalities an event and a function which implements the desired functionality.

###HTML & CSS
To change color and stroke we have to think about how to do it. I decided to work with classes which I will later also use in my JavaScript. With this solution you can add as many colors and styles as you want.

Before we start insert the following into your index.html after the line for the draw.js. Also create the features.js file in the js folder. We will need it later on.
```
<script src="js/features.js"></script>
```

In the server.js file we also need to add a new route to the features.js file
```
app.get('/js/features.js', function(req, res){
    res.sendfile('js/features.js');
});
```

Now we create an unsorted list and the list items we fill with buttons. Each button gets two classes, one for determining which type and one for the color / stroke style.
```
<body>
    <div id="canvas"><!-- Canvas will be created here --></div>
    <div id="buttons">
        <ul class="buttonList">
            <li><input type="button" value="Clear" class="clear" /></li>
            <li>Colors</li>
            <li><input type="button" class="color white"></li>
            <li><input type="button" class="color black" ></li>
            <li><input type="button" class="color pink" /></li>
            <li>Stroke</li>
            <li><input type = "button" class="stroke small"></li>
            <li><input type = "button" class="stroke medium"></li>
            <li><input type = "button" class="stroke big"></li>
        </ul>
    </div>
</body>
```

What we also need are the proper styles for our buttons. We add following styles to our stylesheet:
```
ul.buttonList li {
    list-style-type: none;
    margin-bottom: 10px;
}

#canvas {
    background-color: #fff;
    display: inline;
    float: left;
    margin-right: 20px;
}

#buttons {
    display: inline;
}

.color {
    border: 0px;
    border-radius: 10px;
}

.color:hover {
    border: 2px solid #000;
}

.stroke {
    border: 0px;
    border-radius: 10px;
    background: #000;
}

.stroke:hover {
    border: 2px solid #fff;
}

.color.black {
    background: #000;
}

.color.white{
    background: #fff;
}

.color.pink {
    background: #F824FF;
}

.stroke.small {
    width: 10px;
    height: 10px;
}

.stroke.medium {
    width: 15px;
    height: 15px;
}

.stroke.big {
    width: 20px;
    height: 20px;
}
```

That's all what we have to edit in the HTML and CSS files.

###Additional jQuery manipulations
These are just for completing the design. The first thing is to set the border around the active elements and the second is to remove the dotted lines from the clicked buttons.
```
//Initial border
$('.black').css('border', '2px solid #000');
$('.small').css('border', '2px solid #fff');

//Remove dotted lines from clicked buttons
$('.color, .stroke').on('focus', function(event){
    if(this.blur) this.blur();
});
```

###Clear canvas clientside
To clear the canvas is really easy. We need an event which emits that the canvas is cleared and the clearing method which is really simple and consists of one line code.
```
clearCanvas = function () {
    context.clearRect(0, 0, canvasWidth, canvasHeight);
};

clearCanvasEvent = function() {
    clearCanvas();

    socket.emit('clearCanvas', {
        clear: 'true'
    });
};
```

###Change color clientside
First we need a getClass function which determines the right class from our firing event. It is called later for both, the color and the stroke size.
```
getClass = function (event, className) {
    //Get classes of event firing button
    classList = event.currentTarget.classList;

    //Get new class
    $.each(classList, function(index, value){
        if(value != className){
            newClass = '.' + value;
        }
    });

    return newClass;
};
```

For changing the color we need two new functions. One for emmiting and one for the functionality.
```
changeColor = function (colorClass) {
    //Set new stroke color
    context.strokeStyle = $(colorClass).css('background-color');

    //Border of active color
    $('.color').css('border', '0px');
    $(colorClass).css('border', '2px solid #000');
};

changeColorEvent = function(event) {
    color = getClass(event, 'color');
    changeColor(color);

    socket.emit('changeColor', {
        color: color
    });
};
```

###Change size clientside
As for the color, for the stroke size we also need two functions.

```
changeStroke = function (stroke, strokeClass) {

    //Set new stroke
    context.lineWidth = stroke;

    //Border of active stroke
    $('.stroke').css('border', '0px');
    $(strokeClass).css('border', '2px solid #fff');
};

changeStrokeEvent = function(event) {
    strokeClass = getClass(event, 'stroke');

    //Get right value of class
    stroke = $(strokeClass).css('height');
    stroke = stroke.replace('px', '');

    changeStroke(stroke, strokeClass);

    socket.emit('changeStroke', {
        stroke: stroke,
        strokeClass: strokeClass
    });
}
```

###Clientside listeners
For listening for server messages we need socket listeners for all three functionalities, which simply call the related functions with the received data. We also need simple event listeners for our buttons which call the events we defined above.
```
socket.on('colorEmit', function(data) {
    return changeColor(data.color);
});

socket.on('strokeEmit', function(data) {
    return changeStroke(data.stroke, data.strokeClass);
});

socket.on('clearEmit', function(data) {
    return clearCanvas();
});

//Add Event listeners to buttons
$('.color').on('click', changeColorEvent);
$('.stroke').on('click', changeStrokeEvent);
$('.clear').on('click', clearCanvasEvent);
```

###Serverside
The last step is to add the socket functionality on our server. The whole socket should now look like that:
```
io.sockets.on('connection', function(socket) {

    //Set black color on new connection
    socket.broadcast.emit('colorEmit', {
        color: '.black'
    });

    socket.on('drawClick', function(data) {
        socket.broadcast.emit('draw', {
            x: data.x,
            y: data.y,
            type: data.type
        });
    });
    
    socket.on('changeColor', function(data){
        socket.broadcast.emit('colorEmit', {
            color: data.color
        });
    });

    socket.on('changeStroke', function(data){
        socket.broadcast.emit('strokeEmit', {
            stroke: data.stroke,
            strokeClass: data.strokeClass
        });
    });

    socket.on('clearCanvas', function(data){
        socket.broadcast.emit('clearEmit', {
            clear: 'true'
        });
    });
});
```

When a new connection opens the initial color is set to black, which is sent to all open clients. The other functions simply broadcast the data to all other clients.

##Summary
Canvas is a really amazing new feature of HTML5 which offers the possibility to draw or to create charts without any plugin, just with a bit of JavaScript. To draw with it might be a bit tricky but it is worth the effort.

Socket.io is also a great tool for getting realtime to your application. The realtime canvas app shows that the Socket.io setup is simple and sending and receiving messages with it follows always the same schema. You have an event listener which emits some data to the serverside socket which broadcasts the data to all other clients which listen for it and call the proper functions.

#References:
http://wesbos.com/html5-canvas-websockets-nodejs/

https://github.com/learnboost/socket.io/

http://www.html5canvastutorials.com/