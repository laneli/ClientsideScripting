//General variables
var canvasHeight = 400;
var canvasWidth = 600;

//Create canvas element
var canvas = document.createElement('canvas');
canvas.id = 'myCanvas';
canvas.height = canvasHeight;
canvas.width = canvasWidth;
canvas.draggable = true;
var domCanvas = document.getElementById('canvas');
domCanvas.appendChild(canvas);

//Add 2D context
var context = canvas.getContext('2d');

//Settings for the line
context.fillStyle ='solid';
context.strokeStyle = 'rgb(0, 0, 0)';
context.lineWidth = 10;
context.lineCap = 'round';

//Socket clientside
var socket = io.connect('http://localhost:8000/');

socket.on('draw', function(data) {
	return draw(data.x, data.y, data.type);
});


//Drawing
draw = function(x, y, type) {

	if (type === 'dragstart') {
		context.beginPath();
		return context.moveTo(x, y);
	} else if (type === 'drag') {
		context.lineTo(x, y);
		return context.stroke();
	} else {
		return context.closePath();
	}
};

//Drawing events
$('canvas').on('drag dragstart dragend', function(event) {
	var offset, type, x, y;

	type = event.handleObj.type;
	offset = $(this).offset();
	event.offsetX = event.clientX - offset.left;
    event.offsetY = event.clientY - offset.top;
    x = event.offsetX;
    y = event.offsetY;

    draw(x, y, type);
    socket.emit('drawClick', {
    	x: x,
    	y: y,
    	type: type
    });
});