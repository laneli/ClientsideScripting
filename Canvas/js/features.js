//Initial border
$('.black').css('border', '2px solid #000');
$('.small').css('border', '2px solid #fff');

//Remove dotted lines from clicked buttons
$('.color, .stroke').on('focus', function(event){
	if(this.blur) this.blur();
});


//Clear Canvas Methods

clearCanvas = function () {
	context.clearRect(0, 0, canvasWidth, canvasHeight);
};

clearCanvasEvent = function() {
	clearCanvas();

	socket.emit('clearCanvas', {
		clear: 'true'
	});
};


//Method for getting classes

getClass = function (event, className) {
	//Get classes of event firing button
	classList = event.currentTarget.classList;

	//Get new class
	$.each(classList, function(index, value){
		if(value != className){
			newClass = '.' + value;
		}
	});

	return newClass;
};


//Change color methods

changeColor = function (colorClass) {
	//Set new stroke color
	context.strokeStyle = $(colorClass).css('background-color');

	//Border of active color
	$('.color').css('border', '0px');
	$(colorClass).css('border', '2px solid #000');
};

changeColorEvent = function(event) {
	color = getClass(event, 'color');
	changeColor(color);

	socket.emit('changeColor', {
    	color: color
    });
};


//Change stroke style methods

changeStroke = function (stroke, strokeClass) {

	//Set new stroke
	context.lineWidth = stroke;

	//Border of active stroke
	$('.stroke').css('border', '0px');
	$(strokeClass).css('border', '2px solid #fff');
};

changeStrokeEvent = function(event) {
	strokeClass = getClass(event, 'stroke');

	//Get right value of class
	stroke = $(strokeClass).css('height');
	stroke = stroke.replace('px', '');

	changeStroke(stroke, strokeClass);

	socket.emit('changeStroke', {
		stroke: stroke,
		strokeClass: strokeClass
	});
}


//Socket listeners

socket.on('colorEmit', function(data) {
	return changeColor(data.color);
});

socket.on('strokeEmit', function(data) {
	return changeStroke(data.stroke, data.strokeClass);
});

socket.on('clearEmit', function(data) {
	return clearCanvas();
});

//Add Event listeners to buttons
$('.color').on('click', changeColorEvent);
$('.stroke').on('click', changeStrokeEvent);
$('.clear').on('click', clearCanvasEvent);


