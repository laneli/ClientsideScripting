//App initialization
var express = require('express');
var app = express();
var socket = require('socket.io');

var port = 8000;

//Session handling
app.use(express.cookieParser());
app.use(express.session({secret: 'secret', key: 'express.sid'}));

//Routes
app.get('/', function(req, res){
    res.sendfile('index.html');
});

app.get('/js/draw.js', function(req, res){
    res.sendfile('js/draw.js');
});

app.get('/js/features.js', function(req, res){
	res.sendfile('js/features.js');
});

app.get('/styles.css', function(req, res) {
	res.sendfile('styles.css');
});

app.get('/js/jquery.event.drag-2.0.min.js', function(req, res) {
	res.sendfile('js/jquery.event.drag-2.0.min.js');
});

//Listen for requests
var server = app.listen(port);
var io = socket.listen(server, {log: false});

//Socket
io.sockets.on('connection', function(socket) {

	//Set black color on new connection
	socket.broadcast.emit('colorEmit', {
		color: '.black'
	});

	socket.on('drawClick', function(data) {
		socket.broadcast.emit('draw', {
			x: data.x,
			y: data.y,
			type: data.type
		});
	});
	
	socket.on('changeColor', function(data){
		socket.broadcast.emit('colorEmit', {
			color: data.color
		});
	});

	socket.on('changeStroke', function(data){
		socket.broadcast.emit('strokeEmit', {
			stroke: data.stroke,
			strokeClass: data.strokeClass
		});
	});

	socket.on('clearCanvas', function(data){
		socket.broadcast.emit('clearEmit', {
			clear: 'true'
		});
	});
});

console.log('Server started. Listen on Port 8000');